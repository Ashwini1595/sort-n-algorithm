//Design Code
`include "parameter.sv"

module sortingAlgorithm #(parameter BITS = 1, parameter REGS =1) (input clk, input reset, input [BITS-1:0] N, output reg[BITS-1:0] Q[0:REGS-1]);

  genvar i;
  generate
    for(i=0; i< REGS; i=i+1)
      begin: sortedNum
        if(i==0)
          sort#(BITS) s(clk, reset, N,`BITS'd0,Q[i]);
        else
          sort#(BITS) s(clk, reset, N,Q[i-1],Q[i]);
      end
  endgenerate
endmodule

//To sort
module sort #(parameter B = 1)
  (input clk, reset,input[B-1:0] N, Qprev, output reg[B-1:0] Q);
  wire[B-1:0] D;
  assign D = (N < Q) ? (N < Qprev) ? Qprev : N : Q;
  always @(posedge clk)
    begin
      if(reset)
        Q <= 2**B - 1;
      else
        Q <= D;
    end
endmodule