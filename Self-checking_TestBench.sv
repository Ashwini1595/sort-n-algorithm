`include "parameter.sv"
module sortAlg_tb();
  reg clk, reset;
  reg [`BITS-1:0] N;
  reg[`BITS-1:0] reg_array[0:`REGS-1];
  integer i,j,key;
  wire [`BITS-1:0] R[0:`REGS-1];

  sortingAlgorithm#(`BITS,`REGS) DUT(.clk(clk), .reset(reset), .N(N), .Q(R)); 

  initial
    begin
      #40 N=$urandom%(2**`BITS);
      reg_array[0]= N;
      for(i=1;i<`REGS;i=i+1)
        begin
          #10;
          N=$urandom%(2**`BITS);
          reg_array[i]= N;
        end
      #10 N = `BITS'bz;
      
      for(i=1; i<`REGS; i=i+1)
        begin
          key = reg_array[i];
          for (j=i-1; j>=0 && reg_array[j]>key; j=j-1)
            begin
              reg_array[j+1]=reg_array[j];
            end
          reg_array[j+1]=key;
        end
      $display(" R | reg_array");
      
      for(i=0; i<`REGS; i=i+1)
        begin
          $write("%d | %d", R[i], reg_array[i]);
          if(R[i]==reg_array[i]) $display(" PASS"); else $display(" FAIL");
        end   
  end
  
  
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0);
  end
  
  initial begin
    clk = 0;
    forever #5 clk = ~clk;
  end
  
  initial begin
    #0 reset = 0;
    #20 reset = 1;
    #20 reset = 0;
    #250
    $finish;
  end
endmodule